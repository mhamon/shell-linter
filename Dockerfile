FROM docker.io/alpine:3
# https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL "org.opencontainers.image.created"="$IMAGE_CREATED"
LABEL "org.opencontainers.image.version"="$IMAGE_VERSION"
LABEL "org.opencontainers.image.revision"="$IMAGE_REVISION"
LABEL "maintainer"="$IMAGE_AUTHOR"
LABEL "org.opencontainers.image.authors"="$IMAGE_AUTHOR"
LABEL "org.opencontainers.image.vendor"="Homelab"
LABEL "org.opencontainers.image.licenses"="WTFPL"
LABEL "org.opencontainers.image.url"="$IMAGE_SOURCE"
LABEL "org.opencontainers.image.documentation"="$IMAGE_SOURCE"
LABEL "org.opencontainers.image.source"="$IMAGE_SOURCE"
LABEL "org.opencontainers.image.ref.name"="shellcheck/shfmt"
LABEL "org.opencontainers.image.title"="shellcheck/shfmt"
LABEL "org.opencontainers.image.description"="shellcheck/shfmt"

RUN set -eux && \
    apk update && \
    apk add --no-cache shellcheck shfmt bash
CMD ["/bin/bash"]
